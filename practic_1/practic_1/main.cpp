﻿#include <iostream>
#include <clocale>
using namespace std;

int main()
{
    //Кувшинников Дмитрий
    //КЭ-317
    //2022
    //Программа является учебной
    setlocale(LC_CTYPE, "rus");
    float a, b;

    cout << "Введите первое число a=";
    cin >> a;

    cout << "Введите второе число b=";
    cin >> b;

    cout << "Сумма чисел a+b="<<a+b<<'\n';
    cout << "Разность a-b="<<a-b<<'\n';
    cout << "Произведение a*b="<<a*b<<'\n';
    cout << "Деление чисел a/b="<<a/b<<'\n';

    cin.get();
    
}

